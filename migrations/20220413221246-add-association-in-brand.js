'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addColumn(
      'Brands', // name of source model
      'ClientId', // name of the key we're adding
      {
        type : Sequelize.INTEGER,
        references : {
          model : 'Clients', // name of target model
          key : 'id' // key in target model
        },
        onUpdate : 'CASCADE',
        onDelete : 'SET NULL'
      }
    );
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.removeColumn(
      'Brands',
      'ClientId'
    );
  }
};
