'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addColumn(
      'Addresses', // name of source model
      'CityId', // name of the key we're adding
      {
        type : Sequelize.INTEGER,
        references : {
          model : 'Cities', // name of target model
          key : 'id' // key in target model
        },
        onUpdate : 'CASCADE',
        onDelete : 'SET NULL'
      }
    );
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.removeColumn(
      'Addresses',
      'CityId'
    );
  }
};

