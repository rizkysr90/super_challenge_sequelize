'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addColumn(
      'Addresses', // name of source model
      'CountryId', // name of the key we're adding
      {
        type : Sequelize.INTEGER,
        references : {
          model : 'Countries', // name of target model
          key : 'id' // key in target model
        },
        onUpdate : 'CASCADE',
        onDelete : 'SET NULL'
      }
    );
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.removeColumn(
      'Addresses',
      'CountryId'
    );
  }
};

