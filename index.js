require('dotenv').config();
const express = require('express');
const app = express();
const myAppRouter = require('./src/routes/myApp.route');

app.use(express.json());
app.use(express.urlencoded({extended : true}));
app.use('/', myAppRouter);
app.all('*',(req,res) => {
    res.status(404).send('Url Not Found');
});

/* Error handler middleware */
app.use((err, req, res, next) => {
    console.log(err);
    if (err.name === 'SequelizeForeignKeyConstraintError') {
        res.status(404).json({
            error : {
                name : err.name,
                detail : err.original.detail,
                code : 404
            }
        })
    }
    const statusCode = err.statusCode || 500;
    const statusMessage = err.message || 'Internal Server Error';
    res.status(statusCode).json({
        error : {
            title : err.title,
            message : statusMessage,
            code : statusCode,
        }
    });
    
});
app.listen(3000,() => {
    console.log(`Listening at http:localhost:3000`)
});


