module.exports = {
    emptyData(reqBody) {
        for (props in reqBody) {
            if (reqBody[props] === "") {
                return true;
            }
        }
        return false;
    }
}