module.exports = {
    pagination(page,row) {
        if (page === undefined || page === "") {
            page = 1;
        }
        if (row === undefined || row === "") {
            row = 5;
        }
        page = ((Number(page) - 1) * Number(row));
        return {page,row};
    }
}