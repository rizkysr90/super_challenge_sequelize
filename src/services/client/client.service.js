const { Client } = require('../../../models/index');
const { pagination } = require('../../utility/pagination');
const validator = require('../../validators/requestValidators');
const errorHandling = require('../../models/errorHandling/baseResponse.error')

module.exports = {
    async getAll(req) {
        const {page,row} = pagination(req.page,req.row);
        const options = {
            attributes : {
                exclude : ['createdAt','updatedAt']
            },
            limit : row,
            offset : page,
            order : [['id','ASC']]
        }
        return await Client.findAll(options);
    },
    async createClient(req) {
        // Req Body 
        /* {
                name : yourValue,
                ktp_number : yourValue,
                npwp_number : yourValue
            }
        */
        if (validator.emptyData(req)) {
            throw new errorHandling.InvalidDataError("Field name,ktp_number,npwp_number Is Required")
        }
        const data = await Client.create(req);
        return {
            id : data.id,
            name : data.name,
            ktp_number : data.ktp_number,
            npwp_number : data.npwp_number
        }
    },
    async updateClient(reqParams,reqBody) {
        //Req Params
        /* 
            {
                id : yourValue
            }
        */
        // Req Body 
        /* {
                name : yourValue,
                ktp_number : yourValue,
                npwp_number : yourValue
            }
        */
       isClientFound = await Client.findByPk(reqParams.id);
       if (!isClientFound) {
            throw new errorHandling.NotFoundError('Server cannot find the requested resource')
       };
       if (validator.emptyData(reqBody)) {
            throw new errorHandling.InvalidDataError("Field name,ktp_number,npwp_number Is Required")
       };
       const options = {
           where : {
               id : reqParams.id
           }
       };
       const data = await Client.update(reqBody,options);
       return reqBody;
    },
    async deleteClient(id) {
        isClientFound = await Client.findByPk(id);
        if (!isClientFound) {
                throw new errorHandling.NotFoundError('Server cannot find the requested resource')
        };
        const options = {
            where : {
                id
            }
        }
        const data = await Client.destroy(options);
        return 1;
    }
}