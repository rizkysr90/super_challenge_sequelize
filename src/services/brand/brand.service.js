const { Brand,Client } = require('../../../models/index');
const validator = require('../../validators/requestValidators');
const { pagination } = require('../../utility/pagination');
const fieldsValidator = require('../../validators/fieldsValidators');


module.exports = {
    async createData(req) {
        if (validator.emptyData(req)) {
            throw new errorHandling.InvalidDataError("Field name,is_big_brand,ClientId Is Required")
        }
        const data = await Brand.create(req);
        return {
            id : data.id,
            name : data.name,
            is_big_brand : data.is_big_brand,
            ClientId : data.ClientId
        }
    },
    async getAll(req) {
        const {page,row} = pagination(req.page,req.row);
        const options = {
            attributes : {
                exclude : ['ClientId','createdAt','updatedAt']
            },
            limit : row,
            offset : page,
            order : [['id','ASC']],
            include : [Client]
        }
        return await Brand.findAll(options);
    },
    async updateData(id,reqBody) {
        const fields = fieldsValidator.nullInAssociateColumn(reqBody);
        const options = {
            where : {
                id
            },
            fields
        }
        await Brand.update(reqBody,options);
        return reqBody;
    },
    async deleteData(id) {
        const options = {
            where : {
                id
            }
        }
        await Brand.destroy(options);
        return 1;
    }
}