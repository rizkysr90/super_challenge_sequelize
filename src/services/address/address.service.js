const { Address,Client,City,Province,Country } = require('../../../models/index');
const errorHandling = require('../../models/errorHandling/baseResponse.error'); 
const fieldsValidator = require('../../validators/fieldsValidators');
module.exports = {
    async createAddress(id) {
        return await Address.create({"ClientId" : id});
    },
    async getByClientId(id) {
        const options = {
            where : {
                "ClientId" : id
            },
            include : [Client,City,Province,Country],
            attributes : {
                exclude : ["ClientId","CountryId","ProvinceId","CityId","createdAt","updatedAt"]
            }
        }
        const isFound = await Address.findOne(options)
        if(!isFound) {
            throw new errorHandling.NotFoundError(`Address dengan client_id ${id} tidak ditemukan`);
        }
        return isFound;
    },
    async updateData(id,reqBody) {
        const fields = fieldsValidator.nullInAssociateColumn(reqBody);
        const options = {
            where : {
                "ClientId" : id
            },
            fields
        }
        await Address.update(reqBody,options)
        return reqBody;
    },
    async deleteAddress(id) {
        const options = {
            where : {
                "ClientId" : id
            }
        }
        return await Address.destroy(options);
    }
}