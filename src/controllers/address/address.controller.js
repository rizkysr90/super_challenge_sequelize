const baseResponse = require("../../models/baseResponse/baseResponse");
const addressService = require("../../services/address/address.service");

module.exports = {
    async getByClientId(req,res,next) {
        try {
            const result = await addressService.getByClientId(req.params.client_id);
            res.status(200).json(baseResponse.statusOK(result));
        } catch (error) {
            next(error);
        }
    },
    async updateData(req,res,next) {
        try {
            const resultUpdateData = await addressService.updateData(req.params.client_id,req.body);
            res.status(200).json(baseResponse.statusOK(resultUpdateData));
        } catch (error) {
            next(error);
        }
    }
}