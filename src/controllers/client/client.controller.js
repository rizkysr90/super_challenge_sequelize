const clientService = require('../../services/client/client.service');
const baseResponse = require('../../models/baseResponse/baseResponse');
const addressService = require('../../services/address/address.service');
module.exports = {
    async getAllClient(req,res,next) {
        try {
            const result = await clientService.getAll(req.query);
            res.status(200).json(baseResponse.statusOK(result));
        } catch (error) {
            next(error)
        }
    },
    async createClient(req,res,next) {
        try {
            const result = await clientService.createClient(req.body);
            const resultCreateAddressForClient = await addressService.createAddress(result.id);
            res.status(201).json(baseResponse.created(result));
        } catch (error) {
            next(error);
        }
    },
    async updateClient(req,res,next) {
        try {
            const result = await clientService.updateClient(req.params,req.body);
            res.status(200).json(baseResponse.statusOK(result));
        } catch (error) {
            next(error);
        }
    },
    async deleteClient(req,res,next) {
        try {
            const resultDeleteAddress = await addressService.deleteAddress(req.params.id);
            const result = await clientService.deleteClient(req.params.id);
            res.status(200).json(baseResponse.statusOK(result));
        } catch (error) {
            next(error)
        }
    }
}
