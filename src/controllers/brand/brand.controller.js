const baseResponse = require("../../models/baseResponse/baseResponse");
const brandService = require("../../services/brand/brand.service");

module.exports = {
    async createData(req,res,next) {
        try {
            const result = await brandService.createData(req.body);
            res.status(200).json(baseResponse.statusOK(result));
        } catch (error) {
            next(error);
        }
    },
    async getAll(req,res,next) {
        try {
            const result = await brandService.getAll(req.query);
            res.status(200).json(baseResponse.statusOK(result));
        } catch (error) {
            next(error);
        }
    },
    async updateData(req,res,next) {
        try {
            const result = await brandService.updateData(req.params.brand_id,req.body);
            res.status(200).json(baseResponse.statusOK(result));
        } catch (error) {
            next(error);
        }
    },
    async deleteData(req,res,next) {
        try {
            const result = await brandService.deleteData(req.params.brand_id);
            res.status(200).json(baseResponse.statusOK(result));
        } catch (error) {
            next(error);
        }
    }
}