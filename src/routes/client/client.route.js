const express = require('express');
const router = express.Router();
const clientController = require('../../controllers/client/client.controller');
const addressController = require('../../controllers/address/address.controller');
router.route('/')
    .get(clientController.getAllClient)
    .post(clientController.createClient);

router.route('/:id')
    .put(clientController.updateClient)
    .delete(clientController.deleteClient);

// router.route('/address')
//     .post(addressController.createData)
module.exports = router;