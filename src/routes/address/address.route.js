const router = require('express').Router();
const addressControllers = require('../../controllers/address/address.controller');
router.route('/:client_id')
    .get(addressControllers.getByClientId)
    .put(addressControllers.updateData);


module.exports = router;