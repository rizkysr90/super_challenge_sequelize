const express = require('express');
const router = express.Router();
const clientRouter = require('./client/client.route');
const addressRouter = require('./address/address.route');
const brandRouter = require('./brand/brand.route');

router.get('/',(req,res,next) => res.send('Welcome to Api V1.0'));
router.use('/client',clientRouter);
router.use('/address',addressRouter);
router.use('/brands',brandRouter);
module.exports = router;