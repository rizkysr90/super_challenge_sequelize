const router = require('express').Router();
const brandController = require('../../controllers/brand/brand.controller');
router.route("/")
    .post(brandController.createData)
    .get(brandController.getAll)
router.route("/:brand_id")
    .put(brandController.updateData)
    .delete(brandController.deleteData)
module.exports = router;