class BaseResponse {
    static #model(code,message,data) {
        return {
            code,
            message,
            data
        }
    }
    static statusOK(responseData) {
        return this.#model(200,"Success",responseData);
    }
    static created(responseData) {
       return this.#model(201,"The request succeeded, and a new resource was created as a result",responseData);
    }
}

module.exports = BaseResponse;