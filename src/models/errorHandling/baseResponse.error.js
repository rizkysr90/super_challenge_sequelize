class InvalidDataError extends Error {
    constructor(message) {
        super(message);
        this.title = "Missing Required Data"
        this.statusCode = 400;
    }
}

class NotFoundError extends Error {
    constructor(message) {
        super(message);
        this.title = "Resource Not Found"
        this.statusCode = 404;
    }
}

module.exports =  {
    InvalidDataError,
    NotFoundError
}