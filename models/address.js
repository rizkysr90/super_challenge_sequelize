'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Address extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      const Client = models.Client;
      const Address = models.Address;
      const Country = models.Country;
      const Province = models.Province;
      const City = models.City;

      Client.hasOne(Address);
      Address.belongsTo(Client);
      Country.hasMany(Address);
      Address.belongsTo(Country);
      Province.hasMany(Address);
      Address.belongsTo(Province);
      City.hasMany(Address);
      Address.belongsTo(City);

    }
  }
  Address.init({
    address_description: DataTypes.STRING,
    postal_code: DataTypes.STRING,
    ClientId : DataTypes.INTEGER,
    CountryId : DataTypes.INTEGER,
    ProvinceId : DataTypes.INTEGER,
    CityId : DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Address',
  });
  return Address;
};