'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Brand extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      const Client = models.Client;
      const Brand = models.Brand;
      Client.hasMany(Brand);
      Brand.belongsTo(Client);
    }
  }
  Brand.init({
    name: DataTypes.STRING,
    is_big_brand: DataTypes.BOOLEAN,
    ClientId : DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Brand',
  });
  return Brand;
};