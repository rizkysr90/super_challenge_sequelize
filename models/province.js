'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Province extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      const Country = models.Country;
      const Province = models.Province;
      Country.hasMany(Province);
      Province.belongsTo(Country);
    }
  }
  Province.init({
    name: DataTypes.STRING,
    CountryId : DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Province',
  });
  return Province;
};