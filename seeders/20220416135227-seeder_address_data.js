'use strict';
const addressData = require('../masterdata/address.json');

module.exports = {
  async up (queryInterface, Sequelize) {
      const addressDataMapped = addressData.map((elm) => {
        elm.createdAt = new Date();
        elm.updatedAt = new Date();
        return elm;
      });
      await queryInterface.bulkInsert('Addresses', addressDataMapped,{});
  },

  async down (queryInterface, Sequelize) {
      await queryInterface.bulkDelete('Addresses',null, {truncate:true,restartIdentity:true});
  }
};


