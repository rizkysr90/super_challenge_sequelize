'use strict';
const provinceData = require('../masterdata/province.json');

module.exports = {
  async up (queryInterface, Sequelize) {
      const provinceDataMapped = provinceData.map((elm) => {
        delete elm.ProvinceId;
        delete elm.created_at;
        delete elm.updated_at;

        elm.createdAt = new Date();
        elm.updatedAt = new Date();
        return elm;
      });
      await queryInterface.bulkInsert('Provinces', provinceDataMapped,{});
  },

  async down (queryInterface, Sequelize) {
      await queryInterface.bulkDelete('Provinces',null, {truncate:true,restartIdentity:true});
  }
};
