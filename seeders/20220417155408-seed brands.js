'use strict';
const brandData = require('../masterdata/brands.json');

module.exports = {
  async up (queryInterface, Sequelize) {
      const brandDataMapped = brandData.map((elm) => {
        elm.createdAt = new Date();
        elm.updatedAt = new Date();
        return elm;
      });
      await queryInterface.bulkInsert('Brands', brandDataMapped,{});
  },

  async down (queryInterface, Sequelize) {
      await queryInterface.bulkDelete('Brands',null, {truncate:true,restartIdentity:true});
  }
};