'use strict';
const clientData = require('../masterdata/client.json');

module.exports = {
  async up (queryInterface, Sequelize) {
      const clientDataMapped = clientData.map((elm) => {
        elm.createdAt = new Date();
        elm.updatedAt = new Date();
        return elm;
      });
      await queryInterface.bulkInsert('Clients', clientDataMapped,{});
  },

  async down (queryInterface, Sequelize) {
      await queryInterface.bulkDelete('Clients',null, {truncate:true,restartIdentity:true});
  }
};

