'use strict';
const countryData = require('../masterdata/country.json');

module.exports = {
  async up (queryInterface, Sequelize) {
    const countryDataMapped = countryData.map((elm) => {
      delete elm.country_id;
      delete elm.created_at;
      delete elm.updated_at;

      elm.createdAt = new Date();
      elm.updatedAt = new Date();
      return elm;
    });
   await queryInterface.bulkInsert('Countries', countryDataMapped,{});
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Countries',null, {truncate:true,restartIdentity:true});
  }
};
