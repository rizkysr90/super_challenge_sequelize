'use strict';
const cityData = require('../masterdata/city.json');

module.exports = {
  async up (queryInterface, Sequelize) {
      const cityDataMapped = cityData.map((elm) => {
        delete elm.CityId;
        delete elm.created_at;
        delete elm.updated_at;

        elm.createdAt = new Date();
        elm.updatedAt = new Date();
        return elm;
      });
      await queryInterface.bulkInsert('Cities', cityDataMapped,{});
  },

  async down (queryInterface, Sequelize) {
      await queryInterface.bulkDelete('Cities',null, {truncate:true,restartIdentity:true});
  }
};
